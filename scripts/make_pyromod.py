import shutil
from pathlib import Path
from . import MOD_PATH, check_cwd_is_correct


def make_pyromod():
	shutil.make_archive(MOD_PATH, 'zip', MOD_PATH)


if __name__ == '__main__':
	check_cwd_is_correct()
	make_pyromod()
else:
	raise Exception("Must be called directly")
